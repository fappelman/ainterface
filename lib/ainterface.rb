#
# Monkey patch the standard Module class
class Module
  # Holds all methods that are in an interface
  # for a given interface
  # Use a long name to prevent accidental name conflicts
  @@__interface_methods_per_interface = {}

  # Called by a class to indicate it implements the specified
  # interface(s)
  #
  # @param [AInterface] args One or more interface names that
  #   are being implemented
  #
  # @return [void]
  #
  def implements(*args)
    raise AInterface::Error::MissingArgument if args == []
    # Gather the methods from all specified interfaces
    m = []
    args.each do |module_name|
      raise AInterface::Error::NosuchInterface, module_name.to_s unless
        @@__interface_methods_per_interface[module_name.to_s]
      @@__interface_methods_per_interface[module_name.to_s].each do |method_name|
        m << [module_name.name, method_name]
      end
    end

    # Create a method that checks for the existence
    # of all methods
    t = <<-EOF
      def __check_interface_methods
        #{m}.each do |module_name, method_name|
          next if respond_to? method_name
          raise AInterface::Error::NotImplementedError.new(
                self.class.name,
                method_name,
                module_name
              )
        end
      end
      private :__check_interface_methods
    EOF
    class_eval t unless disabled_interface?

    # Redefine the new method
    t = <<-EOF
      def self.new(*args, &block)
        obj = self.allocate
        obj.send :initialize, *args, &block
        obj.send :__check_interface_methods
        obj
      end
    EOF
    class_eval t unless disabled_interface?

    args.each do |module_name|
      prepend module_name
    end
  end

  # Errors raised here identify the class_name that is not implementing
  # the method required by the Interface.
  #
  #
  # Specifies that an method for an interface must be
  # implemented
  #
  # @param [Symbol] args One ore more methods that need implementation
  #
  # @return [void]
  #
  def must_implement(*args)
    raise AInterface::Error::MissingArgument if args == []
    @@__interface_methods_per_interface[self.name] ||= []
    args.each do |method_name|
      @@__interface_methods_per_interface[self.name] << method_name
    end
  end

  #
  # Checks if the Interface error message must be surpressed
  #
  #
  # @return [Boolean] Return true if error messages should be
  #   surpressed.
  #
  def disabled_interface?
    ENV['DISABLE_RUBY_INTERFACE'] == '1'
  end
  private :disabled_interface?
end

#
# Module AInterface provides some helper classes for the
# abstract interface implementation.
#
# @author Fred Appelman <fred@appelman.net>
#
module AInterface
  #
  # Module Error provides error message handlers for the
  # abstract interface implementation.
  #
  # @author Fred Appelman <fred@appelman.net>
  #
  module Error
    #
    # Class MissingArgument is raised when must_implement or
    # implements is issued without an argument.
    #
    # @author Fred Appelman <fred@appelman.net>
    #
    class MissingArgument < StandardError; end

    #
    # Class NosuchInterface is raised when implements
    # references a Interface that wasn't defined.
    #
    # @author Fred Appelman <fred@appelman.net>
    #
    class NosuchInterface < StandardError; end

    # Raised when a method has not been implemented by a class that
    # has used the implements <InterfaceName> method.
    class NotImplementedError < NoMethodError
      def initialize(class_name, method_name, interface_name)
        super(error_message(class_name, method_name, interface_name), method_name)
      end

      def error_message(class_name, method_name, interface_name)
        "Expected #{class_name} to implement #{method_name} for interface #{interface_name}"
      end
      private :error_message
    end
  end
end
