#
# Module AInterfaceVersion provides a version number
# for the gem AInterface
#
module AInterfaceVersion
  #
  # @return [String] The application version number
  NUMBER = '1.0'
end
