# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ainterface/version'

Gem::Specification.new do |s|
  s.name        = 'ainterface'
  s.version     = AInterfaceVersion::NUMBER
  s.author      = 'Fred Appelman'
  s.date        = Time.now.strftime('%Y-%m-%d')
  s.license     = 'MIT'
  s.homepage    = 'https://bitbucket.org/fappelman/ainterface'
  s.description = 'Provides an abstract interface to Ruby'
  s.summary     = %(
  Provides the concept of an Abstract Interface
  for the Ruby language.
  )
  s.email       = 'rubygems@appelman.net'
  s.files       = Dir['lib/**/*.rb'] \
    + %w[LICENSE.txt README.md]
  s.has_rdoc    = false
end
