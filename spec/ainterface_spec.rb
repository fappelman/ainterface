%w[./lib ../lib . ..].each {|p| $: << p }
require 'ainterface'

describe 'AInterface' do

  it 'Raises error when missing argument for must_implement' do
    expect do
      module MustImplement
        must_implement
      end
    end.to raise_error(AInterface::Error::MissingArgument)
  end

  it 'Raises error when missing argument for implements' do
    expect do
      class IncompleteImplements
        implements
      end
    end.to raise_error(AInterface::Error::MissingArgument)
  end

  it 'Does not raise an error if one argument is given' do
    expect do
      module MustImplement
        must_implement :arg1
      end
    end.not_to raise_error
  end

  it 'Does not raise an error if two arguments are given' do
    expect do
      module MustImplement
        must_implement :arg1, :arg2
      end
    end.not_to raise_error
  end

  it 'Does raise an error if methods are not implemented' do
    expect do
      module MustImplement
        must_implement :arg1, :arg2
      end
      class Implementor
        implements MustImplement
      end
      Implementor.new
    end.to raise_error(AInterface::Error::NotImplementedError)
  end

  it 'Does not raise an error if methods are implemented' do
    expect do
      module MustImplement
        must_implement :arg1, :arg2
      end
      class Implementor
        implements MustImplement
        def arg1
        end
        def arg2
        end
      end
      Implementor.new
    end.not_to raise_error
  end

  it 'Does raise an error if an unknow Interface is implemented' do
    expect do
      module NonExitent
      end
      class Implementor
        implements NonExitent
      end
    end.to raise_error(AInterface::Error::NosuchInterface)
  end

end